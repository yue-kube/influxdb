# Kube-InfluxDB

## Requirements

- A working Kubernetes stack
- A Load Balancer
- A GlusterFS cluster
- An LB dedicated IP range

## How to deploy

Change any values in influxdb-secrets.yml to match your needs.
Change the loadBalancerIP in influxdb-service.yml to match an available IP in the dedicated range.

```bash
kubectl apply -f influxdb-namespace.yml
kubectl apply -f influxdb-config.yml
kubectl apply -f influxdb-secrets.yml
kubectl apply -f influxdb-endpoint.yml
kubectl apply -f influxdb-data.yml
kubectl apply -f influxdb-deployment.yml
kubectl apply -f influxdb-service.yml
```

## Uninstall

```bash
kubectl delete -f influxdb-service.yml
kubectl delete -f influxdb-deployment.yml
kubectl delete -f influxdb-data.yml
kubectl delete -f influxdb-endpoint.yml
kubectl delete -f influxdb-secrets.yml
kubectl delete -f influxdb-config.yml
kubectl delete -f influxdb-namespace.yml
```
