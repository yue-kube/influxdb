---
apiVersion: v1
kind: ConfigMap
metadata:
  name: influxdb-config
  namespace: influxdb
data:
  influxdb.conf: "### Welcome to the InfluxDB configuration file.\n\n# The values in this
    file override the default values used by the system if\n# a config option is not
    specified. The commented out lines are the configuration\n# field and the default
    value used. Uncommenting a line and changing the value\n# will change the value
    used at runtime when the process is restarted.\n\n# Once every 24 hours InfluxDB
    will report usage data to usage.influxdata.com\n# The data includes a random ID,
    os, arch, version, the number of series and other\n# usage data. No data from
    user databases is ever transmitted.\n# Change this option to true to disable reporting.\nreporting-disabled
    = true\n\n# Bind address to use for the RPC service for backup and restore.\n#
    bind-address = \"127.0.0.1:8088\"\n\n###\n### [meta]\n###\n### Controls the parameters
    for the Raft consensus group that stores metadata\n### about the InfluxDB cluster.\n###\n\n[meta]\n
    \ # Where the metadata/raft database is stored\n  dir = \"/var/lib/influxdb/meta\"\n\n
    \ # Automatically create a default retention policy when creating a database.\n
    \ # retention-autocreate = true\n\n  # If log messages are printed for the meta
    service\n  # logging-enabled = true\n\n###\n### [data]\n###\n### Controls where
    the actual shard data for InfluxDB lives and how it is\n### flushed from the WAL.
    \"dir\" may need to be changed to a suitable place\n### for your system, but the
    WAL settings are an advanced configuration. The\n### defaults should work for
    most systems.\n###\n\n[data]\n  # The directory where the TSM storage engine stores
    TSM files.\n  dir = \"/var/lib/influxdb/data\"\n\n  # The directory where the
    TSM storage engine stores WAL files.\n  wal-dir = \"/var/lib/influxdb/wal\"\n\n
    \ # The amount of time that a write will wait before fsyncing.  A duration\n  #
    greater than 0 can be used to batch up multiple fsync calls.  This is useful for
    slower\n  # disks or when WAL write contention is seen.  A value of 0s fsyncs
    every write to the WAL.\n  # Values in the range of 0-100ms are recommended for
    non-SSD disks.\n  # wal-fsync-delay = \"0s\"\n\n\n  # The type of shard index
    to use for new shards.  The default is an in-memory index that is\n  # recreated
    at startup.  A value of \"tsi1\" will use a disk based index that supports higher\n
    \ # cardinality datasets.\n  index-version = \"tsi1\"\n\n  # Trace logging provides
    more verbose output around the tsm engine. Turning\n  # this on can provide more
    useful output for debugging tsm engine issues.\n  # trace-logging-enabled = false\n\n
    \ # Whether queries should be logged before execution. Very useful for troubleshooting,
    but will\n  # log any sensitive data contained within a query.\n  # query-log-enabled
    = true\n\n  # Validates incoming writes to ensure keys only have valid unicode
    characters.\n  # This setting will incur a small overhead because every key must
    be checked.\n  # validate-keys = false\n\n  # Settings for the TSM engine\n\n
    \ # CacheMaxMemorySize is the maximum size a shard's cache can\n  # reach before
    it starts rejecting writes.\n  # Valid size suffixes are k, m, or g (case insensitive,
    1024 = 1k).\n  # Values without a size suffix are in bytes.\n  # cache-max-memory-size
    = \"1g\"\n\n  # CacheSnapshotMemorySize is the size at which the engine will\n
    \ # snapshot the cache and write it to a TSM file, freeing up memory\n  # Valid
    size suffixes are k, m, or g (case insensitive, 1024 = 1k).\n  # Values without
    a size suffix are in bytes.\n  # cache-snapshot-memory-size = \"25m\"\n\n  # CacheSnapshotWriteColdDuration
    is the length of time at\n  # which the engine will snapshot the cache and write
    it to\n  # a new TSM file if the shard hasn't received writes or deletes\n  #
    cache-snapshot-write-cold-duration = \"10m\"\n\n  # CompactFullWriteColdDuration
    is the duration at which the engine\n  # will compact all TSM files in a shard
    if it hasn't received a\n  # write or delete\n  # compact-full-write-cold-duration
    = \"4h\"\n\n  # The maximum number of concurrent full and level compactions that
    can run at one time.  A\n  # value of 0 results in 50% of runtime.GOMAXPROCS(0)
    used at runtime.  Any number greater\n  # than 0 limits compactions to that value.
    \ This setting does not apply\n  # to cache snapshotting.\n  # max-concurrent-compactions
    = 0\n\n  # CompactThroughput is the rate limit in bytes per second that we\n  #
    will allow TSM compactions to write to disk. Note that short bursts are allowed\n
    \ # to happen at a possibly larger value, set by CompactThroughputBurst\n  # compact-throughput
    = \"48m\"\n\n  # CompactThroughputBurst is the rate limit in bytes per second
    that we\n  # will allow TSM compactions to write to disk.\n  # compact-throughput-burst
    = \"48m\"\n\n  # If true, then the mmap advise value MADV_WILLNEED will be provided
    to the kernel with respect to\n  # TSM files. This setting has been found to be
    problematic on some kernels, and defaults to off.\n  # It might help users who
    have slow disks in some cases.\n  # tsm-use-madv-willneed = false\n\n  # Settings
    for the inmem index\n\n  # The maximum series allowed per database before writes
    are dropped.  This limit can prevent\n  # high cardinality issues at the database
    level.  This limit can be disabled by setting it to\n  # 0.\n  # max-series-per-database
    = 1000000\n\n  # The maximum number of tag values per tag that are allowed before
    writes are dropped.  This limit\n  # can prevent high cardinality tag values from
    being written to a measurement.  This limit can be\n  # disabled by setting it
    to 0.\n  # max-values-per-tag = 100000\n\n  # Settings for the tsi1 index\n\n
    \ # The threshold, in bytes, when an index write-ahead log file will compact\n
    \ # into an index file. Lower sizes will cause log files to be compacted more\n
    \ # quickly and result in lower heap usage at the expense of write throughput.\n
    \ # Higher sizes will be compacted less frequently, store more series in-memory,\n
    \ # and provide higher write throughput.\n  # Valid size suffixes are k, m, or
    g (case insensitive, 1024 = 1k).\n  # Values without a size suffix are in bytes.\n
    \ # max-index-log-file-size = \"1m\"\n\n  # The size of the internal cache used
    in the TSI index to store previously \n  # calculated series results. Cached results
    will be returned quickly from the cache rather\n  # than needing to be recalculated
    when a subsequent query with a matching tag key/value \n  # predicate is executed.
    Setting this value to 0 will disable the cache, which may\n  # lead to query performance
    issues.\n  # This value should only be increased if it is known that the set of
    regularly used \n  # tag key/value predicates across all measurements for a database
    is larger than 100. An\n  # increase in cache size may lead to an increase in
    heap usage.\n  series-id-set-cache-size = 100\n\n###\n### [coordinator]\n###\n###
    Controls the clustering service configuration.\n###\n\n[coordinator]\n  # The
    default time a write request will wait until a \"timeout\" error is returned to
    the caller.\n  # write-timeout = \"10s\"\n\n  # The maximum number of concurrent
    queries allowed to be executing at one time.  If a query is\n  # executed and
    exceeds this limit, an error is returned to the caller.  This limit can be disabled\n
    \ # by setting it to 0.\n  # max-concurrent-queries = 0\n\n  # The maximum time
    a query will is allowed to execute before being killed by the system.  This limit\n
    \ # can help prevent run away queries.  Setting the value to 0 disables the limit.\n
    \ # query-timeout = \"0s\"\n\n  # The time threshold when a query will be logged
    as a slow query.  This limit can be set to help\n  # discover slow or resource
    intensive queries.  Setting the value to 0 disables the slow query logging.\n
    \ # log-queries-after = \"0s\"\n\n  # The maximum number of points a SELECT can
    process.  A value of 0 will make\n  # the maximum point count unlimited.  This
    will only be checked every second so queries will not\n  # be aborted immediately
    when hitting the limit.\n  # max-select-point = 0\n\n  # The maximum number of
    series a SELECT can run.  A value of 0 will make the maximum series\n  # count
    unlimited.\n  # max-select-series = 0\n\n  # The maximum number of group by time
    bucket a SELECT can create.  A value of zero will max the maximum\n  # number
    of buckets unlimited.\n  # max-select-buckets = 0\n\n###\n### [retention]\n###\n###
    Controls the enforcement of retention policies for evicting old data.\n###\n\n[retention]\n
    \ # Determines whether retention policy enforcement enabled.\n  # enabled = true\n\n
    \ # The interval of time when retention policy enforcement checks run.\n  # check-interval
    = \"30m\"\n\n###\n### [shard-precreation]\n###\n### Controls the precreation of
    shards, so they are available before data arrives.\n### Only shards that, after
    creation, will have both a start- and end-time in the\n### future, will ever be
    created. Shards are never precreated that would be wholly\n### or partially in
    the past.\n\n[shard-precreation]\n  # Determines whether shard pre-creation service
    is enabled.\n  # enabled = true\n\n  # The interval of time when the check to
    pre-create new shards runs.\n  # check-interval = \"10m\"\n\n  # The default period
    ahead of the endtime of a shard group that its successor\n  # group is created.\n
    \ # advance-period = \"30m\"\n\n###\n### Controls the system self-monitoring,
    statistics and diagnostics.\n###\n### The internal database for monitoring data
    is created automatically if\n### if it does not already exist. The target retention
    within this database\n### is called 'monitor' and is also created with a retention
    period of 7 days\n### and a replication factor of 1, if it does not exist. In
    all cases the\n### this retention policy is configured as the default for the
    database.\n\n[monitor]\n  # Whether to record statistics internally.\n  store-enabled
    = false\n\n  # The destination database for recorded statistics\n  # store-database
    = \"_internal\"\n\n  # The interval at which to record statistics\n  # store-interval
    = \"10s\"\n\n###\n### [http]\n###\n### Controls how the HTTP endpoints are configured.
    These are the primary\n### mechanism for getting data into and out of InfluxDB.\n###\n\n[http]\n
    \ # Determines whether HTTP endpoint is enabled.\n  enabled = true\n\n  # Determines
    whether the Flux query endpoint is enabled.\n  # flux-enabled = false\n\n  # Determines
    whether the Flux query logging is enabled.\n  # flux-log-enabled = false\n\n  #
    The bind address used by the HTTP service.\n  bind-address = \":8086\"\n\n  #
    Determines whether user authentication is enabled over HTTP/HTTPS.\n  auth-enabled
    = false\n\n  # The default realm sent back when issuing a basic auth challenge.\n
    \ # realm = \"InfluxDB\"\n\n  # Determines whether HTTP request logging is enabled.\n
    \ # log-enabled = true\n\n  # Determines whether the HTTP write request logs should
    be suppressed when the log is enabled.\n  # suppress-write-log = false\n\n  #
    When HTTP request logging is enabled, this option specifies the path where\n  #
    log entries should be written. If unspecified, the default is to write to stderr,
    which\n  # intermingles HTTP logs with internal InfluxDB logging.\n  #\n  # If
    influxd is unable to access the specified path, it will log an error and fall
    back to writing\n  # the request log to stderr.\n  # access-log-path = \"\"\n\n
    \ # Filters which requests should be logged. Each filter is of the pattern NNN,
    NNX, or NXX where N is\n  # a number and X is a wildcard for any number. To filter
    all 5xx responses, use the string 5xx.\n  # If multiple filters are used, then
    only one has to match. The default is to have no filters which\n  # will cause
    every request to be printed.\n  # access-log-status-filters = []\n\n  # Determines
    whether detailed write logging is enabled.\n  # write-tracing = false\n\n  # Determines
    whether the pprof endpoint is enabled.  This endpoint is used for\n  # troubleshooting
    and monitoring.\n  # pprof-enabled = true\n\n  # Enables authentication on pprof
    endpoints. Users will need admin permissions\n  # to access the pprof endpoints
    when this setting is enabled. This setting has\n  # no effect if either auth-enabled
    or pprof-enabled are set to false.\n  # pprof-auth-enabled = false\n\n  # Enables
    a pprof endpoint that binds to localhost:6060 immediately on startup.\n  # This
    is only needed to debug startup issues.\n  # debug-pprof-enabled = false\n\n  #
    Enables authentication on the /ping, /metrics, and deprecated /status\n  # endpoints.
    This setting has no effect if auth-enabled is set to false.\n  # ping-auth-enabled
    = false\n\n  # Determines whether HTTPS is enabled.\n  # https-enabled = false\n\n
    \ # The SSL certificate to use when HTTPS is enabled.\n  # https-certificate =
    \"/etc/ssl/influxdb.pem\"\n\n  # Use a separate private key location.\n  # https-private-key
    = \"\"\n\n  # The JWT auth shared secret to validate requests using JSON web tokens.\n
    \ # shared-secret = \"\"\n\n  # The default chunk size for result sets that should
    be chunked.\n  # max-row-limit = 0\n\n  # The maximum number of HTTP connections
    that may be open at once.  New connections that\n  # would exceed this limit are
    dropped.  Setting this value to 0 disables the limit.\n  # max-connection-limit
    = 0\n\n  # Enable http service over unix domain socket\n  # unix-socket-enabled
    = false\n\n  # The path of the unix domain socket.\n  # bind-socket = \"/var/run/influxdb.sock\"\n\n
    \ # The maximum size of a client request body, in bytes. Setting this value to
    0 disables the limit.\n  # max-body-size = 25000000\n\n  # The maximum number
    of writes processed concurrently.\n  # Setting this to 0 disables the limit.\n
    \ # max-concurrent-write-limit = 0\n\n  # The maximum number of writes queued
    for processing.\n  # Setting this to 0 disables the limit.\n  # max-enqueued-write-limit
    = 0\n\n  # The maximum duration for a write to wait in the queue to be processed.\n
    \ # Setting this to 0 or setting max-concurrent-write-limit to 0 disables the
    limit.\n  # enqueued-write-timeout = 0\n\n\t# User supplied HTTP response headers\n\t#\n\t#
    [http.headers]\n\t#   X-Header-1 = \"Header Value 1\"\n\t#   X-Header-2 = \"Header
    Value 2\"\n\n###\n### [logging]\n###\n### Controls how the logger emits logs to
    the output.\n###\n\n[logging]\n  # Determines which log encoder to use for logs.
    Available options\n  # are auto, logfmt, and json. auto will use a more a more
    user-friendly\n  # output format if the output terminal is a TTY, but the format
    is not as\n  # easily machine-readable. When the output is a non-TTY, auto will
    use\n  # logfmt.\n  # format = \"auto\"\n\n  # Determines which level of logs
    will be emitted. The available levels\n  # are error, warn, info, and debug. Logs
    that are equal to or above the\n  # specified level will be emitted.\n  # level
    = \"info\"\n\n  # Suppresses the logo output that is printed when the program
    is started.\n  # The logo is always suppressed if STDOUT is not a TTY.\n  # suppress-logo
    = false\n\n###\n### [subscriber]\n###\n### Controls the subscriptions, which can
    be used to fork a copy of all data\n### received by the InfluxDB host.\n###\n\n[subscriber]\n
    \ # Determines whether the subscriber service is enabled.\n  # enabled = true\n\n
    \ # The default timeout for HTTP writes to subscribers.\n  # http-timeout = \"30s\"\n\n
    \ # Allows insecure HTTPS connections to subscribers.  This is useful when testing
    with self-\n  # signed certificates.\n  # insecure-skip-verify = false\n\n  #
    The path to the PEM encoded CA certs file. If the empty string, the default system
    certs will be used\n  # ca-certs = \"\"\n\n  # The number of writer goroutines
    processing the write channel.\n  # write-concurrency = 40\n\n  # The number of
    in-flight writes buffered in the write channel.\n  # write-buffer-size = 1000\n\n\n###\n###
    [[graphite]]\n###\n### Controls one or many listeners for Graphite data.\n###\n\n[[graphite]]\n
    \ # Determines whether the graphite endpoint is enabled.\n  # enabled = false\n
    \ # database = \"graphite\"\n  # retention-policy = \"\"\n  # bind-address = \":2003\"\n
    \ # protocol = \"tcp\"\n  # consistency-level = \"one\"\n\n  # These next lines
    control how batching works. You should have this enabled\n  # otherwise you could
    get dropped metrics or poor performance. Batching\n  # will buffer points in memory
    if you have many coming in.\n\n  # Flush if this many points get buffered\n  #
    batch-size = 5000\n\n  # number of batches that may be pending in memory\n  #
    batch-pending = 10\n\n  # Flush at least this often even if we haven't hit buffer
    limit\n  # batch-timeout = \"1s\"\n\n  # UDP Read buffer size, 0 means OS default.
    UDP listener will fail if set above OS max.\n  # udp-read-buffer = 0\n\n  ###
    This string joins multiple matching 'measurement' values providing more control
    over the final measurement name.\n  # separator = \".\"\n\n  ### Default tags
    that will be added to all metrics.  These can be overridden at the template level\n
    \ ### or by tags extracted from metric\n  # tags = [\"region=us-east\", \"zone=1c\"]\n\n
    \ ### Each template line requires a template pattern.  It can have an optional\n
    \ ### filter before the template and separated by spaces.  It can also have optional
    extra\n  ### tags following the template.  Multiple tags should be separated by
    commas and no spaces\n  ### similar to the line protocol format.  There can be
    only one default template.\n  # templates = [\n  #   \"*.app env.service.resource.measurement\",\n
    \ #   # Default template\n  #   \"server.*\",\n  # ]\n\n###\n### [collectd]\n###\n###
    Controls one or many listeners for collectd data.\n###\n\n[[collectd]]\n  # enabled
    = false\n  # bind-address = \":25826\"\n  # database = \"collectd\"\n  # retention-policy
    = \"\"\n  #\n  # The collectd service supports either scanning a directory for
    multiple types\n  # db files, or specifying a single db file.\n  # typesdb = \"/usr/local/share/collectd\"\n
    \ #\n  # security-level = \"none\"\n  # auth-file = \"/etc/collectd/auth_file\"\n\n
    \ # These next lines control how batching works. You should have this enabled\n
    \ # otherwise you could get dropped metrics or poor performance. Batching\n  #
    will buffer points in memory if you have many coming in.\n\n  # Flush if this
    many points get buffered\n  # batch-size = 5000\n\n  # Number of batches that
    may be pending in memory\n  # batch-pending = 10\n\n  # Flush at least this often
    even if we haven't hit buffer limit\n  # batch-timeout = \"10s\"\n\n  # UDP Read
    buffer size, 0 means OS default. UDP listener will fail if set above OS max.\n
    \ # read-buffer = 0\n\n  # Multi-value plugins can be handled two ways.\n  # \"split\"
    will parse and store the multi-value plugin data into separate measurements\n
    \ # \"join\" will parse and store the multi-value plugin as a single multi-value
    measurement.\n  # \"split\" is the default behavior for backward compatibility
    with previous versions of influxdb.\n  # parse-multivalue-plugin = \"split\"\n###\n###
    [opentsdb]\n###\n### Controls one or many listeners for OpenTSDB data.\n###\n\n[[opentsdb]]\n
    \ # enabled = false\n  # bind-address = \":4242\"\n  # database = \"opentsdb\"\n
    \ # retention-policy = \"\"\n  # consistency-level = \"one\"\n  # tls-enabled
    = false\n  # certificate= \"/etc/ssl/influxdb.pem\"\n\n  # Log an error for every
    malformed point.\n  # log-point-errors = true\n\n  # These next lines control
    how batching works. You should have this enabled\n  # otherwise you could get
    dropped metrics or poor performance. Only points\n  # metrics received over the
    telnet protocol undergo batching.\n\n  # Flush if this many points get buffered\n
    \ # batch-size = 1000\n\n  # Number of batches that may be pending in memory\n
    \ # batch-pending = 5\n\n  # Flush at least this often even if we haven't hit
    buffer limit\n  # batch-timeout = \"1s\"\n\n###\n### [[udp]]\n###\n### Controls
    the listeners for InfluxDB line protocol data via UDP.\n###\n\n[[udp]]\n  # enabled
    = false\n  # bind-address = \":8089\"\n  # database = \"udp\"\n  # retention-policy
    = \"\"\n\n  # InfluxDB precision for timestamps on received points (\"\" or \"n\",
    \"u\", \"ms\", \"s\", \"m\", \"h\")\n  # precision = \"\"\n\n  # These next lines
    control how batching works. You should have this enabled\n  # otherwise you could
    get dropped metrics or poor performance. Batching\n  # will buffer points in memory
    if you have many coming in.\n\n  # Flush if this many points get buffered\n  #
    batch-size = 5000\n\n  # Number of batches that may be pending in memory\n  #
    batch-pending = 10\n\n  # Will flush at least this often even if we haven't hit
    buffer limit\n  # batch-timeout = \"1s\"\n\n  # UDP Read buffer size, 0 means
    OS default. UDP listener will fail if set above OS max.\n  # read-buffer = 0\n\n###\n###
    [continuous_queries]\n###\n### Controls how continuous queries are run within
    InfluxDB.\n###\n\n[continuous_queries]\n  # Determines whether the continuous
    query service is enabled.\n  # enabled = true\n\n  # Controls whether queries
    are logged when executed by the CQ service.\n  # log-enabled = true\n\n  # Controls
    whether queries are logged to the self-monitoring data store.\n  # query-stats-enabled
    = false\n\n  # interval for how often continuous queries will be checked if they
    need to run\n  # run-interval = \"1s\"\n\n###\n### [tls]\n###\n### Global configuration
    settings for TLS in InfluxDB.\n###\n\n[tls]\n  # Determines the available set
    of cipher suites. See https://golang.org/pkg/crypto/tls/#pkg-constants\n  # for
    a list of available ciphers, which depends on the version of Go (use the query\n
    \ # SHOW DIAGNOSTICS to see the version of Go used to build InfluxDB). If not
    specified, uses\n  # the default settings from Go's crypto/tls package.\n  # ciphers
    = [\n  #   \"TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305\",\n  #   \"TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305\",\n
    \ # ]\n\n  # Minimum version of the tls protocol that will be negotiated. If not
    specified, uses the\n  # default settings from Go's crypto/tls package.\n  # min-version
    = \"tls1.2\"\n\n  # Maximum version of the tls protocol that will be negotiated.
    If not specified, uses the\n  # default settings from Go's crypto/tls package.\n
    \ # max-version = \"tls1.3\"\n"
